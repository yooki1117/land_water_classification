# Land_Water_Classification

## This is Python scripts to classify dry land and water Area.
 
ISO Unsupervised Classification, NDMI: Normalized Difference Moisture Index (Water) and table and scatter plot with the line will be generated to compare dry land, moisture land, and water.

| Original                         | NDMI: Normalized Difference Moisture Index (Water)|
| :------------------------------: | :----------------------------: |
| ![original](Image/original.JPG) | ![recoded](Image/ndmi.JPG) |
| Classified 25                    | Result : Land, Sea             |
| ![original](Image/classified25.JPG) | ![recoded](Image/result.JPG) |

See the [wiki](https://gitlab.com/yooki1117/land_water_classification/wikis/home) for more info.

## Create NDMI: Normalized Difference Moisture Index (Water)

NDMI is calculated as a ratio between the NIR and SWIR values in traditional fashion.

### Formula

(NIR - SWIR) / (NIR + SWIR)

In Landsat 4-7,

NDMI = (Band 4 – Band 5) / (Band 4 + Band 5).

In Landsat 8,

NDMI = (Band 5 – Band 6) / (Band 5 + Band 6).

## Create Classification Toolbox 

This toolbox work same with ISO Unsupervised Classification. It will create a classified raster and signature file (.gsg).

![original](Image/ISOCL.JPG)
1. Input image that will be classified
2. Number of Class - a Set class size that you want
3. Minimum class size (optional) - the basic setting is 20
4. Sample Interval (optional) - basic setting is 10
5. Output signature file (optional) - Set location and name for signature file(.gsg)
6. Output - set output location for classified raster 

## Convert gsg file to csv file

Convert .gsg file to .csv file so we can open in the Excel.

With the .csv file, we can create a table and scatterplot with the line.

 ![original](Image/scatter.JPG)
